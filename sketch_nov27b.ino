int x_max;
int y_max;
int z_max;
int i=0;
int aaaa;
//
// Limit Switches
//
#define X_MIN_PIN          3
#define X_MAX_PIN          2
#define Y_MIN_PIN          14
#define Y_MAX_PIN          15
#define Z_MIN_PIN          18
#define Z_MAX_PIN          19

//
// Steppers
//
#define X_STEP_PIN         54
#define X_DIR_PIN          55
#define X_ENABLE_PIN       38
#define X_CS_PIN           53

#define Y_STEP_PIN         60
#define Y_DIR_PIN          61
#define Y_ENABLE_PIN       56
#define Y_CS_PIN           49

#define Z_STEP_PIN         46
#define Z_DIR_PIN          48
#define Z_ENABLE_PIN       62
#define Z_CS_PIN           40

#define E0_STEP_PIN        26
#define E0_DIR_PIN         28
#define E0_ENABLE_PIN      24
#define E0_CS_PIN          42

void setup() 
{
  pinMode(X_MAX_PIN,INPUT);
  pinMode(Y_MAX_PIN,INPUT);
  pinMode(Z_MAX_PIN,INPUT);
  
  pinMode(X_STEP_PIN,OUTPUT);
  pinMode(X_DIR_PIN,OUTPUT);
  pinMode(X_ENABLE_PIN,OUTPUT);
  pinMode(X_CS_PIN,OUTPUT);
  
  pinMode(Y_STEP_PIN,OUTPUT);
  pinMode(Y_DIR_PIN,OUTPUT);
  pinMode(Y_ENABLE_PIN,OUTPUT);
  pinMode(Y_CS_PIN,OUTPUT);
  
  pinMode(Z_STEP_PIN,OUTPUT);
  pinMode(Z_DIR_PIN,OUTPUT);
  pinMode(Z_ENABLE_PIN,OUTPUT);
  pinMode(Z_CS_PIN,OUTPUT);
  
  digitalWrite(X_MAX_PIN,HIGH);
  digitalWrite(Y_MAX_PIN,HIGH);
  digitalWrite(Z_MAX_PIN,HIGH);
  
  digitalWrite(X_ENABLE_PIN,LOW);
  digitalWrite(X_DIR_PIN,LOW);
  digitalWrite(X_CS_PIN,HIGH);
  
  digitalWrite(Y_ENABLE_PIN,LOW);
  digitalWrite(Y_DIR_PIN,LOW);
  digitalWrite(Y_CS_PIN,HIGH);
  
  digitalWrite(Z_ENABLE_PIN,LOW);
  digitalWrite(Z_DIR_PIN,LOW);
  digitalWrite(Z_CS_PIN,HIGH);
  
  Serial.begin(115200);
}

int x=0,y=0,z=0;
String value="";

void loop() 
{
  x_max=digitalRead(X_MAX_PIN);
  y_max=digitalRead(Y_MAX_PIN);
  z_max=digitalRead(Z_MAX_PIN);

  char a[30];
  int i=0;
  bool tempX=false,tempY=false,tempZ=false;
  bool inverseX=false,inverseY=false,inverseZ=false;
  while(Serial.available())
  {
    delayMicroseconds(75);
    if(!tempX && !tempY && !tempZ)
    {
      x=0;
      y=0;
      z=0;
    }
    
    a[i] = Serial.read();
    if(a[i]=='x') 
    {
      tempX=true;
      tempY=false;
      tempZ=false;
      value ="";
    }
    else if(a[i]=='y') 
    {
      tempY=true;
      tempX=false;
      tempZ=false;
      value ="";
    }
    else if(a[i]=='z') 
    {
      tempZ=true;
      tempX=false;
      tempY=false;
      value ="";
    }

    if(tempX)
    {
      if(a[i]>='0' && a[i]<='9') 
      {
        value = value + a[i];
        x=value.toInt();
      }
      else if(a[i]=='-') inverseX=true;
    }
    else if(tempY)
    {
      if(a[i]>='0' && a[i]<='9') 
      {
        value = value + a[i];
        y=value.toInt();
      }
      else if(a[i]=='-') inverseY=true;
    }
    else if(tempZ)
    {
      if(a[i]>='0' && a[i]<='9') 
      {
        value = value + a[i];
        z=value.toInt();
      }
      else if(a[i]=='-') inverseZ=true;
    }
    
    i++;
  }

  if(inverseX) 
  {
    digitalWrite(X_DIR_PIN,HIGH);
  }
  else
  {
    digitalWrite(X_DIR_PIN,LOW);
  }
  
  if(inverseY) 
  {
    digitalWrite(Y_DIR_PIN,HIGH);
  }
  else
  {
    digitalWrite(Y_DIR_PIN,LOW);
  }
  
  if(inverseZ) 
  {
    digitalWrite(Z_DIR_PIN,HIGH);
  }
  else
  {
    digitalWrite(Z_DIR_PIN,LOW);
  }
bool asd = false;
  while(x||y||z)
  {
    
          x_max=digitalRead(X_MAX_PIN);
          if(x_max!=0) 
          {
            x=100;
            digitalWrite(X_DIR_PIN,HIGH);
          }

          y_max=digitalRead(Y_MAX_PIN);
          if(y_max!=0) 
          {
            y=100;
            digitalWrite(Y_DIR_PIN,HIGH);
          }
          
          z_max=digitalRead(Z_MAX_PIN);
          if(z_max!=0) 
          {
            z=100;
            digitalWrite(Z_DIR_PIN,HIGH);
          }

          if(x>0) digitalWrite(X_STEP_PIN,HIGH);
          if(y>0) digitalWrite(Y_STEP_PIN,HIGH);
          if(z>0) digitalWrite(Z_STEP_PIN,HIGH);   
          
          delayMicroseconds(500);
          
          if(x>0) 
          {
            digitalWrite(X_STEP_PIN,LOW);
            x--;
          }
          if(y>0) 
          {
            digitalWrite(Y_STEP_PIN,LOW);
            y--;
          }
          if(z>0) 
          {
            digitalWrite(Z_STEP_PIN,LOW);
            z--;
          }

          if((asd==false)&&(abs(x)<20||abs(y)<20||abs(z)<20))
          {
            Serial.println("done");
            asd=true;
          }
          delayMicroseconds(500);
  }  
}
